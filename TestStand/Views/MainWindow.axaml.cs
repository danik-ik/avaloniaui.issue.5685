using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Markup.Xaml.Styling;

namespace TestStand.Views
{
    public class MainWindow : Window
    {
        private readonly StyleInclude[] _styles =
        {
            CreateStyle("avares://Avalonia.Themes.Fluent/FluentDark.xaml"),
            CreateStyle("avares://Avalonia.Themes.Fluent/FluentLight.xaml"),
        };

        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            var selector = this.Find<ComboBox>("Themes"); 
            selector.SelectionChanged += (sender, e) =>
            {
                Styles[0] = (_styles[selector.SelectedIndex]);
            };

            Styles.Add(_styles[0]); 
        }

        private static StyleInclude CreateStyle(string url)
        {
            var self = new Uri("resm:Styles?assembly=TestStand");
            return new StyleInclude(self)
            {
                Source = new Uri(url)
            };
        }

    }
}