# AvaloniaUI issue 5685

https://github.com/AvaloniaUI/Avalonia/issues/5685

Attempt to localize the source of the error.

Previously, I found a similar error when re-opening the ComboBox after changing the theme

## Tag: 1

The exception is reproduced with minimal interface, without external dependencies.

Note: there is a difference between the two methods of loading a theme (app.axaml and code: Styles.Add(new StyleInclude(…)); ) without downloading one of the Fluent theme in the the App.axaml app crashes on startup.

But with Citrus.Avalonia themes there is not same problem.



